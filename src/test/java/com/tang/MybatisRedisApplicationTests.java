package com.tang;

import com.tang.entity.User;
import com.tang.service.UserService;
import com.tang.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MybatisRedisApplicationTests {

    @Autowired
    private UserService userService;

    @Test
    void contextLoads() {
        List<User> userAll = userService.getUserAll();
        for (User user : userAll) {
            System.out.println(user);
        }
        System.out.println("=================");

    }

}
