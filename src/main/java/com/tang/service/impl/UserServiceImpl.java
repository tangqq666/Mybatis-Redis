package com.tang.service.impl;

import com.tang.dao.UserMapper;
import com.tang.entity.User;
import com.tang.service.UserService;
import com.tang.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public List<User> getUserAll() {
        //先从redis找有没有数据
        List<User> users=(List<User>)redisUtil.get("users");
        if (users == null){
            //没有就去数据库查询
            users = userMapper.getUserAll();
            //再将值赋给redis
            redisUtil.set("users",users);
        }
        return users;
    }
}
