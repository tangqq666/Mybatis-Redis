package com.tang.service;

import com.tang.entity.User;

import java.util.List;

public interface UserService {
    List<User> getUserAll();
}
